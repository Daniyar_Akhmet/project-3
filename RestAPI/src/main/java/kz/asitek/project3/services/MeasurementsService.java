package kz.asitek.project3.services;

import kz.asitek.project3.models.Measurements;
import kz.asitek.project3.models.Sensor;
import kz.asitek.project3.repository.MeasurementsRepository;
import kz.asitek.project3.repository.SensorRepository;
import kz.asitek.project3.util.MeasurementsNotCreatedException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MeasurementsService {

    private final MeasurementsRepository measurementsRepository;
    private final SensorRepository sensorRepository;


    @Transactional
    public void save(Measurements measurements) {

        enrichment(measurements);
        measurementsRepository.save(measurements);
    }

    @Transactional(readOnly = true)
    public List<Measurements> findAll() {
        return measurementsRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Integer rainyDaysCount() {
        return measurementsRepository.rainyDaysCount();
    }

    private void enrichment(Measurements measurements) {
        measurements.setCreatedAt(LocalDateTime.now());

        Optional<Sensor> sensor = sensorRepository.findSensorByName(measurements.getSensor().getName());

        if (sensor.isPresent()) {
            measurements.setSensor(sensor.get());
        } else throw new MeasurementsNotCreatedException("Sensor not exists!");

    }


}
