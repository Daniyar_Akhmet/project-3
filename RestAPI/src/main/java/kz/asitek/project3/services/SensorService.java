package kz.asitek.project3.services;

import kz.asitek.project3.dto.SensorDTO;
import kz.asitek.project3.models.Sensor;
import kz.asitek.project3.repository.SensorRepository;
import kz.asitek.project3.util.SensorAlreadyExistsException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SensorService {

    private final SensorRepository sensorRepository;

    @Transactional
    public void save(Sensor sensor) throws SensorAlreadyExistsException {
        Optional<Sensor> sensorByName = sensorRepository.findSensorByName(sensor.getName());
        if (sensorByName.isPresent()) throw new SensorAlreadyExistsException("Sensor already exists!");
        sensorRepository.save(sensor);
    }
}
