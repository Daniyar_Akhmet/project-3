package kz.asitek.project3.models;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "Sensor")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Sensor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name")
    @NotEmpty(message = "Name should be not empty")
    @Size(min = 2, max = 30, message = "Name should between 2 and 30 characters")
    private String name;

//    @OneToMany(mappedBy = "sensor")
//    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
//    private List<Measurements> measurementsList;
}
