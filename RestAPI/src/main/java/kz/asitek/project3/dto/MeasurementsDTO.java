package kz.asitek.project3.dto;

import kz.asitek.project3.models.Sensor;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MeasurementsDTO {
    @NotNull
    @Min(value = -100)
    @Max(value = 100)
    private double value;

    @NotNull
    private boolean raining;

    @NotNull
    private Sensor sensor;

}
