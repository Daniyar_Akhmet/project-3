package kz.asitek.project3.controllers;

import kz.asitek.project3.dto.MeasurementsDTO;
import kz.asitek.project3.models.Measurements;
import kz.asitek.project3.services.MeasurementsService;
import kz.asitek.project3.util.MeasurementsNotCreatedException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/measurements")
@RequiredArgsConstructor
public class MeasurementsController {

    private final MeasurementsService measurementsService;
    private final ModelMapper modelMapper;


    @PostMapping("/add")
    public ResponseEntity<HttpStatus> add(@RequestBody @Valid MeasurementsDTO measurementsDTO,
                                          BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            StringBuilder msg = new StringBuilder();

            List<FieldError> errors = bindingResult.getFieldErrors();

            for (FieldError error : errors) {
                msg.append(error.getField())
                        .append(" - ")
                        .append(error.getDefaultMessage())
                        .append(";");
            }

            throw new MeasurementsNotCreatedException(msg.toString());
        }

        measurementsService.save(convertToMeasurements(measurementsDTO));

        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping()
    public List<MeasurementsDTO> findAll() {
        return measurementsService.findAll()
                .stream()
                .map(this::convertToMeasurementsDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/rainyDaysCount")
    public Integer rainyDaysCount() {
        return measurementsService.rainyDaysCount();
    }

    private Measurements convertToMeasurements(MeasurementsDTO measurementsDTO) {
        return modelMapper.map(measurementsDTO, Measurements.class);
    }

    private MeasurementsDTO convertToMeasurementsDTO(Measurements measurements) {
        return modelMapper.map(measurements, MeasurementsDTO.class);
    }
}
