package kz.asitek.project3.util;

public class MeasurementsNotCreatedException extends RuntimeException{
    public MeasurementsNotCreatedException(String msg) {super(msg);}
}
