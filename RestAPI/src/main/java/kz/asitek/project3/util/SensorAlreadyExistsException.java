package kz.asitek.project3.util;

public class SensorAlreadyExistsException extends  Exception{
    public SensorAlreadyExistsException(String msg) {super(msg);}
}
