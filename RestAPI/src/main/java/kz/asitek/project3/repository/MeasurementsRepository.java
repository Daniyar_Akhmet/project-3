package kz.asitek.project3.repository;

import kz.asitek.project3.models.Measurements;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MeasurementsRepository extends JpaRepository<Measurements, Integer> {
    @Query("select count(*) from Measurements where raining = true")
    public Integer rainyDaysCount();
}
