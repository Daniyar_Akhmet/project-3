create table Sensor (
                        id int generated by default as identity primary key,
                        name varchar(50) unique
);

create table Measurements (
                              id int generated by default as identity primary key,
                              value numeric(5,2),
                              raining bool,
                              created_at timestamp,
                              sensor_id int references Sensor(id)
);