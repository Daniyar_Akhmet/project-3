package org.example.services;

import org.apache.commons.math3.util.Precision;
import org.example.models.Measurements;
import org.example.models.Sensor;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Random;

public class AddMeasurements {
    public static void add1000Measurements() {

        Sensor sensor = new AddSensor().addSensor();

        String url = "http://localhost:8080/measurements/add";
        RestTemplate restTemplate = new RestTemplate();

        for (int i = 0; i < 1000; i++) {
            Measurements measurements = new Measurements();
            double temperature = (new Random().nextDouble()) * 200 - 100;
            measurements.setValue(Precision.round(temperature, 2));
            measurements.setRaining(List.of(false, true).get(new Random().nextInt(2)));
            measurements.setSensor(sensor);

            HttpEntity<Measurements> request = new HttpEntity<>(measurements);
            restTemplate.postForObject(url, request, String.class);
        }
    }
}
