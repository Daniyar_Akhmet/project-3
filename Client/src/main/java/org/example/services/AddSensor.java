package org.example.services;

import org.example.models.Sensor;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class AddSensor {
    public Sensor addSensor() {

        System.out.println("Enter sensor name: ");
        String sensorName = new Scanner(System.in).nextLine();

        Sensor sensor = new Sensor(sensorName);

        HttpEntity<Sensor> request = new HttpEntity<>(sensor);

        RestTemplate restTemplate = new RestTemplate();

        restTemplate.postForObject("http://localhost:8080/sensor/registration", request, String.class);

        return sensor;
    }
}
