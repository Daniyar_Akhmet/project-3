package org.example.services;

import org.example.models.Measurements;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GetMeasurements {
    public static List<Measurements> get1000Measurements() {

        RestTemplate restTemplate = new RestTemplate();

        Measurements[] forObject = restTemplate.getForObject("http://localhost:8080/measurements", Measurements[].class);

        return Arrays.stream(forObject).limit(1000).collect(Collectors.toList());
    }
}
