package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.example.models.Measurements;
import org.example.services.AddMeasurements;
import org.example.services.GetMeasurements;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import java.util.List;


public class App {
    public static void main(String[] args) throws JsonProcessingException {

        AddMeasurements.add1000Measurements();

        List<Measurements> measurements = GetMeasurements.get1000Measurements();

        double[] valueY = new double[measurements.size()];
        double[] valueX = new double[measurements.size()];
        int i = 0;

        for (Measurements item : measurements) {
            valueY[i] = measurements.get(i).getValue();
            valueX[i] = i;
            i++;
        }

        XYChart chart = QuickChart.getChart("Sample Chart", "X", "Y", "y(x)", valueX, valueY);

        new SwingWrapper(chart).displayChart();
    }
}
