package org.example.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Measurements {
    private double value;
    private boolean raining;
    private Sensor sensor;
}
